class PostsController < ApplicationController

  public def index
    @posts = Post.all
  end

  public def new
    @post = Post.new
  end

  public def show
    @post = Post.find(params[:id])
  end

  public def create
    @post = Post.new(post_params)

    if @post.save
      redirect_to @post
    else
      render 'new'
    end
  end

  public def edit
    @post = Post.find(params[:id])
  end

  public def update
    @post = Post.find(params[:id])

    if @post.update(post_params)
      redirect_to @post
    else
      render 'new'
    end
  end

  public def destroy
    @post = Post.find(params[:id])
    @post.destroy

    redirect_to posts_path
  end

  private def post_params
    params.require(:post).permit(:title, :body)
  end

end
