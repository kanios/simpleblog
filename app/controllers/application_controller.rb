class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :get_admin_perm

  public def get_admin_perm
    @admin_perm = true
  end

end
